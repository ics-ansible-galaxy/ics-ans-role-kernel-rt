---
- name: remove unnecessary packages
  yum:
    name:
      - postfix
      - sendmail
      - cups
    state: absent

- name: make sure irqbalance is stopped and disabled
  systemd:
    name: irqbalance
    state: stopped
    enabled: false

- name: install dkms and required packages
  yum:
    name:
      - dkms
      - gcc
      - make
      - numactl-devel
      - tuna
      - tuned
      - tuned-profiles-realtime
      - rt-setup
    state: present

- name: exclude kernel from yum update
  lineinfile:
    state: present
    path: /etc/yum.conf
    regexp: "^exclude="
    insertafter: '^\[main\]'
    line: "exclude=kernel* centos-release*"

- name: set isolated_cores to {{ kernel_rt_isolated_cores }}
  lineinfile:
    state: present
    path: /etc/tuned/realtime-variables.conf
    regexp: "^isolated_cores="
    line: "isolated_cores={{ kernel_rt_isolated_cores }}"
  notify: reboot the server

- name: check tuned profile
  command: "tuned-adm active"
  check_mode: false
  changed_when: false
  register: tuned_active_profile

- name: set tuned profile to realtime
  command: "tuned-adm profile realtime"
  when: "'profile: realtime' not in tuned_active_profile.stdout"
  notify: reboot the server

- name: install real-time kernel {{ kernel_rt_version }}
  yum:
    name:
      - "kernel-rt-{{ kernel_rt_version }}"
      - "kernel-rt-devel-{{ kernel_rt_version }}"
    state: present
    disable_excludes: main
  notify: reboot the server

- name: get full kernel path
  shell: "ls /boot/vmlinuz-{{ kernel_rt_version }}*"
  check_mode: false
  changed_when: false
  register: kernel_path

- name: check kernel boot information
  command: "grubby --info {{ kernel_path.stdout }}"
  check_mode: false
  changed_when: false
  register: kernel_boot_info

- name: check kernel saved entry
  shell: |
    set -o pipefail
    grub2-editenv list | grep saved_entry
  check_mode: false
  changed_when: false
  register: saved_entry

- name: set {{ kernel_rt_version }} as default kernel
  command: "grubby --set-default {{ kernel_path.stdout }}"
  when: "kernel_rt_version not in saved_entry.stdout"
  notify: reboot the server

- name: update kernel boot information
  command: "grubby --update-kernel={{ kernel_path.stdout }} --args {{ item }}"
  when: "item not in kernel_boot_info.stdout"
  loop: "{{ kernel_rt_boot_parameters }}"
  notify: reboot the server

- name: ensure dkms service is enabled and started
  systemd:
    name: dkms
    enabled: true
    state: started

- name: flush handlers to reboot on the proper kernel
  meta: flush_handlers
